#include <iostream>
#include <sorts.h>
#include <vector>
#include "lib/lab05/inc/expression.h"

int main()
{
    std::vector<unsigned> test = {3,1,7,5,9,6,10,2,4,7};
    std::vector<unsigned> test2 = {1,3,5,7,9,2,4,6,8,10};
    std::vector<unsigned> test3 = {72, 96, 26, 94, 33, 2, 42, 89, 41, 36, 63, 65, 56, 16, 17, 59, 35, 39, 40, 1, 69, 100};
    std::vector<unsigned> test4 = {15,22,13,4,2,16,42,65,5,12};

    lab6::doubly_linked_list nums(test);
    lab6::doubly_linked_list nums2(test2);
    lab6::doubly_linked_list nums3(test3);
    lab6::doubly_linked_list nums4(test4);
    sorts sortingmachine;

//    nums = sortingmachine.quick_sort(nums);
//    nums.print();

//    nums2 = sortingmachine.quick_sort(nums3);
//    nums2.print();

    nums3 = sortingmachine.radix_sort(nums3);
    nums3.print();

//    nums4 = sortingmachine.quick_sort(nums4);
//    nums4.print();

    return 1;
}