//
// Created by Bryan on 9/30/2017.
//

#include <iostream>
#include "../inc/expression.h"

namespace lab5
{
    expression::expression() = default;

    expression::expression(std::string &input_expression)
    {
        convert_to_postfix(input_expression);
        remove_space(input_expression);
    }

    void expression::convert_to_postfix(std::string &input_expression)
    {
        while (input_expression.length() != 0)
        {
            std::string next = input_expression.substr(0, 1);
            if (is_number(next))
            {
                int len = 1;
                bool reachedOp = false;
                for (int i = 1; (i < input_expression.length() && !reachedOp); i++)
                {
                    std::string nextchar = input_expression.substr(i, 1);
                    if (is_number(nextchar))
                        len++;
                    else if (is_operator(nextchar))
                        reachedOp = true;
                }
                next = input_expression.substr(0, len);
                get_number(next);
                input_expression = input_expression.substr(len);
            }
            else if (is_operator(next))
            {
                get_operator(next);
                input_expression = input_expression.substr(1);

            }
            else if (is_space(next))
            {
                // ignore it
                input_expression = input_expression.substr(1);
            }
            else
            {
                std::cout << " character " << next << " is not readable" << std::endl;
            }
        }
        if (!operators.isEmpty())
        {
            while (!operators.isEmpty())
            {
                std::string top = operators.top();
                postfix.enqueue(top);
                operators.pop();
            }
        }
    }

    void expression::parse_to_infix(std::string &input_expression)
    {

//        while (input_expression.length() != 0)
//        {
//            std::string next = input_expression.substr(0, 1);
//            if (is_number(next))
//            {
//                int len = 1;
//                bool reachedOp = false;
//                for (int i = 1; (i < input_expression.length() && !reachedOp); i++)
//                {
//                    std::string nextchar = input_expression.substr(i, 1);
//                    if (is_number(nextchar))
//                        len++;
//                    else if (is_operator(nextchar))
//                        reachedOp = true;
//                }
//                next = input_expression.substr(0, len);
//                get_number(next);
//                input_expression = input_expression.substr(len);
//            }
//            else if (is_operator(next))
//            {
//                get_operator(next);
//                input_expression = input_expression.substr(1);
//
//            }
//            else if (is_space(next))
//            {
//                // ignore it
//                input_expression = input_expression.substr(1);
//            }
//            else
//            {
//                std::cout << " character " << next << " is not readable" << std::endl;
//            }
//        }
    }

// push every number you see to the stack
// when you get to an operator
// pop two numbers off of the stack and do the appropriate operation
// push the result onto the stack
    int expression::calculate_postfix()
    {
        stack nums;
        while (postfix.queueSize() > 0)
        {
            std::string next = postfix.top();
            if (is_number(next))
            {
                nums.push(next);
                postfix.dequeue();
            }
            else if (is_operator(next))
            {
                std::string sec = nums.top();
                nums.pop();
                std::string first = nums.top();
                nums.pop();
                std::string calc = std::to_string(calculate_operation(next, sec, first));
                nums.push(calc);
                postfix.dequeue();
            }
        }
        return std::stoi(nums.top());
    }

    void expression::print_infix()
    {
        queue temp = no_space;
        temp.setSize(no_space.getSize());
        while (!temp.isEmpty())
        {
            std::cout << temp.top() << " ";
            temp.dequeue();
        }
//        std::cout << "\n";
    }

    void expression::print_postfix()
    {
        queue temp = postfix;
        temp.setSize(postfix.getSize());
        while (!temp.isEmpty())
        {
            std::cout << temp.top() << " ";
            temp.dequeue();
        }
        std::cout << "\n";
    }

    expression::expression(expression &rhs)
    {

    }
    std::istream &operator>>(std::istream &steam, expression &RHS)
    {
        
        return steam;
    }

    bool expression::is_space(std::string input)
    {
        if (input == " ")
            return true;
        return false;
    }

    bool expression::is_number(std::string input_string)
    {
        char in = input_string[0];
        if ((in - '0') >= 0 && (in - '0') < 11)
            return true;
        return false;
    }

    bool expression::is_operator(std::string input_string)
    {
        if (input_string == "/" || input_string == "*" || input_string == "+" || input_string == "-")
            return true;
        else if (input_string == "(" || input_string == ")")
            return true;
        return false;
    }

    int expression::get_number(std::string input_string)
    {
        postfix.enqueue(input_string);
        return 0;
    }
    void expression::remove_space(std::string input_string)
    {
        while (input_string.length() != 0)
        {
            std::string next = input_string.substr(0, 1);
            if (!is_space(next))
            {
                no_space.enqueue(next);
            }
        }

    }
    queue expression::getnoSpace()
    {
        return no_space;
    }

    std::string expression::get_operator(std::string input_string)
    {
        if (is_left_parenthesis(input_string))
        {
            operators.push(input_string);
        }
        else if (is_right_parenthesis(input_string))
        {
            while (!is_left_parenthesis(operators.top()) && operators.stackSize() != 0)
            {
                std::string a = operators.top();
                postfix.enqueue(a);
                operators.pop();
            }
            operators.pop();
        }
        else if (is_operator(input_string))
        {
            // add to operator stack after checking precedence
            if (operators.isEmpty())
            {
                operators.push(input_string);
            }
            else                                      // determine where it needs to be inserted in the stack
            {
                while (!operators.isEmpty() &&
                       operator_precedence(operators.top()) >= operator_precedence(input_string))
                {
                    std::string a = operators.top();
                    postfix.enqueue(a);
                    operators.pop();
                }
                operators.push(input_string);
            }
        }
        else
            std::cout << "Get_operator failed, " << input_string << " is not an operator" << std::endl;
        return "";
    }

    int expression::operator_precedence(std::string operator_in)
    {
        if (is_operator(operator_in))
        {
            if (operator_in == "*" || operator_in == "/")
                return 10;
            else if (operator_in == "-" || operator_in == "+")
                return 5;
            else
                return 0;
        }
        else
        {
            std::cout << "operator_in " << operator_in << " is not an operator";
            return -1;
        }
    }

    int expression::calculate_operation(std::string operator_from_stack, std::string operand_from_stack_2,
                                        std::string operand_from_stack_1)
    {
        int first = std::stoi(operand_from_stack_1, nullptr, 0);
        int second = std::stoi(operand_from_stack_2, nullptr, 0);
        int ans;
        if (operator_from_stack == "+")
        {
            ans = first + second;
            return ans;
        }
        else if (operator_from_stack == "-")
        {
            ans = first - second;
            return ans;
        }
        else if (operator_from_stack == "*")
        {
            ans = first * second;
            return ans;
        }
        else if (operator_from_stack == "/")
        {
            ans = first / second;
            return ans;
        }
        else
        {
            std::cout << "operator not recognized... \n";
        }
    }

    bool expression::is_left_parenthesis(std::string operators)
    {
        return operators == "(";
    }

    bool expression::is_right_parenthesis(std::string operators)
    {
        return operators == ")";
    }

    /*
    int expression::calculate_infix()
    {
        // setup stacks used for calculation
        stack operators;
        stack operands;

        // copy infix queue into a local queue for use in the calculation
        queue input_queue = infix;

        // Created temporary value used for storing the current expression
        std::string expression_value;
        std::string operator_from_stack;
        std::string operand_from_stack_1;
        std::string operand_from_stack_2;
        std::string calculation_result;
        // Run through the queue until it is empty.
        while (!input_queue.isEmpty())
        {
            // Grab and remove the top value of the input queue and store it in a string.
            expression_value = input_queue.top();
            input_queue.dequeue();

            // Check to see if the value is a number, if it is push it to the operands stack
            if (is_number(expression_value))
            {
                operands.push(expression_value);
            }
                // If the expression is an operator, and the operators stack is empty, push the operator to the operators stack
            else if (operators.isEmpty() && is_operator(expression_value))
            {
                operators.push(expression_value);
            }
                // If the expression is an operator and the top of the operators stack is less than the current operator, push
                // push the operator to the operators stack
            else if ((is_operator(operators.top()) < operator_precedence(expression_value))
                     && is_operator(expression_value))
            {
                operators.push(expression_value);
            }
                // If the expression in a left parenthesis, it pushes it onto the operators stack
            else if (is_left_parenthesis(expression_value))
            {
                operators.push(expression_value);
            }
                // If the expression is a right parenthesis, we need to calculate the value inside the parenthesis, and push
                // it onto the operand stack
            else if (is_right_parenthesis(expression_value))
            {
                // Run until we get to the left parenthesis
                while (!is_left_parenthesis(operators.top()))
                {
                    // Pop operator off of operators stack
                    operator_from_stack = operators.top();
                    operators.pop();
                    // Pop operands off of operands stack
                    operand_from_stack_1 = operands.top();
                    operands.pop();
                    operand_from_stack_2 = operands.top();
                    operands.pop();
                    // Calculate given operation
                    calculation_result = calculate_operation(operator_from_stack, operand_from_stack_2,
                                                             operand_from_stack_1);
                    // Push result to operand stack
                    operands.push(calculation_result);
                }
                // Get rid of left parenthesis
                operators.pop();
            }
                // If any of the stuff above isn't true, then we know we need to calculate some values
            else
            {
                while (!operators.isEmpty() &&
                       (operator_precedence(operators.top()) >= operator_precedence(expression_value)))
                {
                    // Pop operator off of operators stack
                    operator_from_stack = operators.top();
                    operators.pop();
                    // Pop operands off of operands stack
                    operand_from_stack_1 = operands.top();
                    operands.pop();
                    operand_from_stack_2 = operands.top();
                    operands.pop();
                    // Calculate given operation
                    calculation_result = calculate_operation(operator_from_stack, operand_from_stack_2,
                                                             operand_from_stack_1);
                    // Push result to operand stack
                    operands.push(calculation_result);
                }
                //Push the operator to the operators stack
                operators.push(expression_value);
            }
        }
        // Process the rest of the stack until there is nothing left in the operators stack
        while (!operators.isEmpty())
        {
            // Pop operator off of operators stack
            operator_from_stack = operators.top();
            operators.pop();
            // Pop operands off of operands stack
            operand_from_stack_1 = operands.top();
            operands.pop();
            operand_from_stack_2 = operands.top();
            operands.pop();
            // Calculate given operation
            calculation_result = calculate_operation(operator_from_stack, operand_from_stack_2,
                                                     operand_from_stack_1);
            // Push result to operand stack
            operands.push(calculation_result);
        }
        std::string final_value = operands.top();
        return std::stoi(final_value);
    }
     */
}