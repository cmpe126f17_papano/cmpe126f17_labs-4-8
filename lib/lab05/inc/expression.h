#ifndef CMPE126F17_LABS_LIB_LAB05_INC_EXPRESSION_H
#define CMPE126F17_LABS_LIB_LAB05_INC_EXPRESSION_H

#include "queue.h"
#include "stack.h"

namespace lab5
{
    class expression
    {
        queue infix;
        queue postfix;
        queue nums;     // the number queue
        stack operators;// the operator stack
        queue no_space;

        void convert_to_postfix(std::string &input_expression);
        void parse_to_infix(std::string &input_expression);
        bool is_number(std::string input_string);
        bool is_operator(std::string input_string);
        int get_number(std::string input_string);
        std::string get_operator(std::string input_string);
        int operator_precedence(std::string operator_in);
        int calculate_operation(std::string operator_from_stack, std::string operand_from_stack_2, std::string operand_from_stack_1);
        bool is_left_parenthesis(std::string operators);
        bool is_right_parenthesis(std::string operators);
        bool is_space(std::string input);
        void remove_space(std::string input_string);
        queue getnoSpace();
        expression(expression &rhs);
    public:
        expression();
        expression(std::string &input_expression);
        int calculate_postfix();
        void print_infix();
        void print_postfix();
        friend std::istream& operator>>(std::istream& steam, expression& RHS);
    };
}

#endif //CMPE126F17_LABS_LIB_LAB05_INC_EXPRESSION_H
