#include "../inc/tree.h"
#include <iostream>

namespace lab7
{
void clear(node *to_clear);

void insert_recursive(node *temp, int value);

void print_recursive(node *temp);

bool check_val_recursive(int key, node *temp, bool found);

bool exists_recursive(node *temp, int key);

int get_frequency_recursive(node *temp, int key);

unsigned depth_recursive(node *temp);

void print_path_recursive(node *temp, int key);

int find_level_recursive(node *temp, int key, int depth);

void remove_recursive(node *temp, node *tempAbove, int key);

void node_print_gtl(node *top);



/*
 * Help on wikipedia: https://en.wikipedia.org/wiki/Binary_search_tree
 * Rule of the tree: left = less, right = more
 */

// Construct an empty tree
tree::tree()
{
    root = nullptr;
    sizeT = 0;
}

// Deconstruct tree
tree::~tree()
{
    clear(root);
}

// Insert
void tree::insert(int value) // calls insert recursive
{
    if (root == nullptr)
    {
        root = new node(value);
    }
    else
    {
        node *temp = root;
        insert_recursive(temp, value);
    }
    sizeT++;
}

void insert_recursive(node *temp, int value) // recursively moves down tree to find place for value
{
    if (temp != nullptr && temp->right == nullptr || temp->left == nullptr)
    {
        if (temp->data < value && temp->right == nullptr)
            temp->right = new node(value);
        else if (temp->data > value && temp->left == nullptr)
            temp->left = new node(value);
        else
        {
            if (temp->data == value)
                temp->frequency++;
            else if (temp->data < value)
                insert_recursive(temp->right, value);
            else
                insert_recursive(temp->left, value);
        }
    }
    else if (temp->right->data == value)
    {
        temp->right->frequency++;
    }
    else if (temp->left->data == value)
    {
        temp->left->frequency++;
    }
    else
    {
        if (temp->data < value)
            insert_recursive(temp->right, value);
        else
            insert_recursive(temp->left, value);
    }
}

// Remove key
bool tree::remove(int key)
{
    if (sizeT == 0)
    {
        delete root;
    }
    if (in_tree(key))
    {
        node *temp = root;
        node *tempAbove = temp;
        remove_recursive(temp, tempAbove, key);
        sizeT--;
        return true;
    }
    else
    {
        return false;
//            throw "key does not exist in tree";
    }

}

void remove_recursive(node *temp, node *tempAbove, int key)
{
    if (temp != nullptr && temp->data == key)
    {
        if (temp->left != nullptr && temp->right != nullptr) // 2 children
        {
            node *findSmall = temp->right;
            while (findSmall->left != nullptr) // find smallest value in right subtree
            {
                findSmall = findSmall->left;

            }
            temp->data = findSmall->data;
            tempAbove = temp;
            temp = temp->right;
            remove_recursive(temp, tempAbove, findSmall->data);
        }
        else if (temp->left != nullptr || temp->right != nullptr) // 1 child
        {
            if (temp->left != nullptr)
            {
                if (tempAbove->left == temp && temp->frequency == 1)
                {
                    tempAbove->left = temp->left;
                }
                else if (tempAbove->right == temp && temp->frequency == 1)
                {
                    tempAbove->right = temp->left;
                }
            }
            else
            {
                if (tempAbove->right == temp && temp->frequency == 1)
                {
                    tempAbove->right = temp->right;
                }
                else if (tempAbove->left == temp && temp->frequency == 1)
                {
                    tempAbove->left = temp->right;
                }
            }
            if (temp->frequency == 1)
            {
                delete temp;
            }
            else
            {
                temp->frequency--;
            }

        }
        else // no children
        {
            if (tempAbove->left == temp && temp->frequency == 1)
            {
                tempAbove->left = nullptr;
            }
            else if (tempAbove->right == temp && temp->frequency == 1)
            {
                tempAbove->right = nullptr;
            }
            if (temp->frequency == 1)
            {
                delete temp;
            }
            else
            {
                temp->frequency--;
            }
        }
    }
    else
    {
        tempAbove = temp;
        if (temp->data < key)
        {
            temp = temp->right;
        }
        else
        {
            temp = temp->left;
        }
        remove_recursive(temp, tempAbove, key);
    }
}


// What level is key on?
int tree::level(int key) //recursive
{
    node *temp = root;
    return find_level_recursive(temp, key, 0);
}

int find_level_recursive(node *temp, int key, int depth)
{
    if (temp == nullptr)
        return -1;

    if (temp->data == key)
    {
        return depth;
    }
    else
    {
        if (find_level_recursive(temp->left, key, depth + 1) > depth)
            return find_level_recursive(temp->left, key, depth + 1);
        else
            return find_level_recursive(temp->right, key, depth + 1);
    }
}

// Print the path to the key, starting with root
void tree::path_to(int key)
{
    if (in_tree(key))
    {
        node *temp = root;
        print_path_recursive(temp, key);
        std::cout << "\n";
    }
    else
    {

    }
}

void print_path_recursive(node *temp, int key)
{

    if (temp == nullptr)
    {}
    else if (temp->data == key)
    {
        std::cout << temp->data;
    }
    if (temp->left != nullptr)
    {
        if (key < temp->data)
        {
            std::cout << temp->data << " -> ";
            print_path_recursive(temp->left, key);
        }
    }
    if (temp->right != nullptr)
    {
        if (key > temp->data)
        {
            std::cout << temp->data << " -> ";
            print_path_recursive(temp->right, key);
        }

    }
}

// Number of items in the tree
unsigned tree::size() // returns tree size
{
    return sizeT;
}

// Calculate the depth of the tree, longest string of connections
unsigned tree::depth()
{
    node *temp = root;
    if (root == nullptr)
        return 0;
    return depth_recursive(temp) - 1;
}

unsigned depth_recursive(node *temp)
{
    if (temp == nullptr)
        return 0;
    else
    {
        unsigned left_depth = depth_recursive(temp->left);
        unsigned right_depth = depth_recursive(temp->right);
        if (left_depth > right_depth)
            return left_depth + 1;
        return right_depth + 1;
    }

}

// Determine whether the given key is in the tree
bool tree::in_tree(int key)
{
    node *temp = root;
    return exists_recursive(temp, key);
}

bool exists_recursive(node *temp, int key)
{
    if (temp == nullptr)
    {}
    else if (temp->data == key)
        return true;
    else
    {
        if (exists_recursive(temp->left, key))
        {
            return exists_recursive(temp->left, key);
        }
        else
        {
            return exists_recursive(temp->right, key);
        }

    }
}

// Return the number of times that value is in the tree
int tree::get_frequency(int key)
{
    if (in_tree(key))
    {
        node *temp = root;
        return get_frequency_recursive(temp, key);
    }
    else
    {
        return 0;
    }
}

int get_frequency_recursive(node *temp, int key)
{
    if (temp == nullptr)
    {
        return 0;
    }
    else if (temp->data == key)
        return temp->frequency;
    else
    {
        if (get_frequency_recursive(temp->left, key) != 0)
            return get_frequency_recursive(temp->left, key);
        else
            return get_frequency_recursive(temp->right, key);
    }
}

// Print the tree least to greatest, Include duplicates
void tree::print()
{
    node *temp = root;
    print_recursive(temp);
    std::cout << "\n";
}

void print_recursive(node *temp)
{
    if (temp == nullptr)
    {}
    else
    {
        print_recursive(temp->left);
        for (int i = 0; i < temp->frequency; i++)
            std::cout << temp->data << " ";
        print_recursive(temp->right);
    }
}

void tree::print_gtl()
{
    node_print_gtl(root);
    std::cout << std::endl;
}

void node_print_gtl(node *top)
{
    if (top == nullptr) return;
    node_print_gtl(top->right);
    for (int i = 0; i < top->frequency; i++) std::cout << top->data << " ";
    node_print_gtl(top->left);
}

void clear(node *to_clear)
{
    if (to_clear == nullptr) return;
    if (to_clear->left != nullptr) clear(to_clear->left);
    if (to_clear->right != nullptr) clear(to_clear->right);
    delete to_clear;
}

}