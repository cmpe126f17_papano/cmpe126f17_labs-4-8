#include "doubly_linked_list.h"
/*
 * You will be writing all of the code for each of these functions.
 * Remember, this is a doubly linked list, not an array. You need to
 * be using pointers, and not overwriting your values like you would
 * in an array.
 *
 * If you need to write auxiliary functions, you are more than welcome
 * to, but you can't change the signature of any of the functions we
 * have written.
 *
 * Information on doubly linked lists can be found at
 * https://en.wikipedia.org/wiki/Doubly_linked_list
 *
 * Hints: - Keep track of size. If you add or delete something, you
 *          need to change size.
 *        - This isn't an array, so moving things around is actually
 *          a lot easier. Just change the pointers to the objects.
 *        - Keep track of your edge cases; empty, 1 item, and 2 items
 *        - Some of these functions are basically the same thing,
 *          with the index shifted or return ignored. Don't rewrite
 *          code, just call the function with the 'correct' inputs.
 *        - Test your doubly linked list by itself before testing it
 *          in the deck class. It will make it easier to find any bugs
 *        - Use your debugger. It is your best friend for finding
 *          issues!
 *        - Don't forget to ask for help on Slack!
 *
 * We will be making changes throughout the week to the deck.cpp as
 * well as adding testing into the project. Make sure to pull and
 * merge frequently.
 */


namespace lab6
{
    // Default constructor
    doubly_linked_list::doubly_linked_list()
    {
        head = nullptr;
        tail = nullptr;
        size = 0;
    }

// Take in a vector of inputs and construct a doubly linked list from them
    doubly_linked_list::doubly_linked_list(std::vector<unsigned> values)
    {
        head = new node(values[0]);
        tail = head;
        size++;
        for (int i = 1; i < values.size(); i++)
        {
            append(values[i]);
        }
        size = values.size();
    }

// Copy constructor
    doubly_linked_list::doubly_linked_list(const doubly_linked_list &original)
    {
        if (original.size > 0)
        {
            head = new node(original.head->data);
            head->prev = nullptr;
            tail = head;
            node *temp = original.head;
            while (temp->next != nullptr)
            {
                temp = temp->next;
                append(temp->data);
            }
        }
        size = original.size;
    }

// Create doubly linked linked list with one input value
    doubly_linked_list::doubly_linked_list(unsigned input)
    {
        head = new node(input);
        tail = head;
        size = 1;
    }

// Default destructor
    doubly_linked_list::~doubly_linked_list()
    {
        node *temp = head;
        while (head != nullptr)
        {
            head = head->next;
            delete temp;
            temp = head;
        }
    }

// return the value inside of the node located at position
    unsigned doubly_linked_list::get_data(unsigned position)
    {
        if (position < 0) // checks for valid input
        {
//            std::cout << "Invalid position, must be greater than 0" << std::endl;
        }
        node *temp = nullptr;
        int i = 0;
        if (lessThanMidpoint(position)) // finds location if less than midpoint of linked list
        {
            temp = head;
            while (i != position)
            {
                if (temp->next != nullptr)
                {
                    temp = temp->next;
                    i++;
                } else
                {
//                    throw "Invalid position, out of bounds";
                }
            }
        }
        else
        {
            temp = tail;
            i = size-1;
            while (i != position)
            {
                if (temp->prev != nullptr)
                {
                    temp = temp->prev;
                    i--;
                }
                else
                {
//                    throw "Invalid position, out of bounds";
                }
            }
        }
        return temp->data;
    }

// Get a set of values between position_from to position_to
    std::vector<unsigned> doubly_linked_list::get_set(unsigned position_from, unsigned position_to)
    {
        if (position_to < position_from)
        {
//            throw "position error: position_from greater than position_to";
            unsigned hold = position_from;
            position_from = position_to;
            position_to = hold;
        }
        std::vector<unsigned> to_return;
        node *temp = nullptr;
        int i = 0;
        if (lessThanMidpoint(position_from))    //finds location if less than midpoint of linked list
        {
            temp = head;
            while (i != position_from)
            {
                temp = temp->next;
                i++;
            }
        }
        else
        {
            temp = tail;
            i = size - 1;
            while (i != position_from)
            {
                temp = temp->prev;
                i--;
            }
        }
        for (int i = 0; i < position_to - position_from + 1; i++) // adds values to vector before returning
        {
            if (temp!= nullptr)
                to_return.push_back(temp->data);
            if(temp->next != nullptr)
                temp = temp->next;
        }
        return to_return;

    }

// Add a value to the end of the list
    void doubly_linked_list::append(unsigned data)
    {
        if (head == nullptr && tail == nullptr) // list has 0 elements
        {
            head = new node(data);
            tail = head;
        }
        else
        {
            tail->next = new node(data);
            tail->next->prev = tail;
            tail = tail->next;
        }
        size++;
    }

// Merge two lists together in place, placing the input list at the end of this list
    void doubly_linked_list::merge(doubly_linked_list rhs)
    {
        node *temp = rhs.head;
        while (temp->next != nullptr) // adds them on to the end
        {
            this->append(temp->data);
            temp = temp->next;
        }
        this->append(temp->data);
    }

    // Allow for the merging of two lists using the + operator.
    doubly_linked_list doubly_linked_list::operator+(const doubly_linked_list &rhs) const
    {
        doubly_linked_list to_return;
        to_return.merge(*this);
        to_return.merge(rhs);
        return to_return;
    }

// Insert a node before the node located at position
    void doubly_linked_list::insert_before(unsigned position, unsigned data)
    {
        if (position < 0 || position > size)
        {
//            throw "position out of bounds";
        }
        int i = 0;
        node *temp = nullptr;
        if (position == 0) // adds a new node and moves head
        {
            node *add = new node(data);
            add->next = head;
            head->prev = add;
            head = add;
        }
        else if (lessThanMidpoint(position))    //finds location if less than midpoint of linked list
        {
            temp = head;
            i = 0;
            position--;
            while (i < position && temp->next != nullptr)
            {
                temp = temp->next;
                i++;
            }
            node *add = new node(data);
            add->next = temp->next;
            add->prev = temp;
            temp->next = add;
        }
        else
        {
            temp = tail;
            i = size-1;
            while (i > position && temp->prev != nullptr)
            {
                temp = temp->prev;
                i--;
            }
            node *add = new node(data);
            add->next = temp->next;
            add->prev = temp;
            temp->next = add;
        }
        size++;
    }

// Insert a node after the node located at position
    void doubly_linked_list::insert_after(unsigned position, unsigned data)
    {
        if (position == size)
        {
            append(data);
        } else
        {
            insert_before(position + 1, data);
        }
    }

// Remove the node located at position from the linked list
    void doubly_linked_list::remove(unsigned position)
    {
        if (position < 0 || position >= size)
        {
//            throw "size out of bounds";
        }
        int i = 0;
        node *temp = nullptr;
        if (position == 0) // edge case, position is head
        {
            temp = head;
            head = head->next;
            if (head != nullptr)
                head->prev = nullptr;
            delete temp;
        }
        else if (position == size - 1) // edge case, position is tail
        {
            temp = tail;
            tail = tail->prev;
            tail->next = nullptr;
            delete temp;
        }
        else if (lessThanMidpoint(position))  //finds location if less than midpoint of linked list
        {
            temp = head;
            while (i < position && temp->next != nullptr)
            {
                temp = temp->next;
                i++;
            }
            temp->prev->next = temp->next;
            temp->next->prev = temp->prev;
            delete temp;
        }
        else
        {
            temp = tail;
            while (i > position && temp->prev != nullptr)
            {
                temp = temp->prev;
                i--;
            }
            temp->prev->next = temp->next;
            temp->next->prev = temp->prev;
            delete temp;
        }
        size--;
    }

    // Split the list with the node being split on being included in the returned list
    doubly_linked_list doubly_linked_list::split_before(unsigned position)
    {
        if (position <= 0 || position >= size)
        {
//            throw "Out of bounds!";
        }
        doubly_linked_list to_return;
        node *temp = head;
        for (int i = 0; i < position && temp->next != nullptr; i++)
        {
            temp = temp->next;
        }
        while (temp != nullptr)
        {
            to_return.append(temp->data);
            temp = temp->next;
        }
        to_return.size = (position-size)*-1;
        return to_return;
    }

    // Split the list with the node being split on being included in the retained list
    doubly_linked_list doubly_linked_list::split_after(unsigned position)
    {
        if (position < 0 || position > size)
        {    return *this;
        }
        else
        {
            return split_before(position+1);
        }
    }

    // Create two lists, one starting at position_from and ending with position_to and return that list
    // Merge the beginning of the original list with the end of the original list and retain it
    doubly_linked_list doubly_linked_list::split_set(unsigned position_from, unsigned position_to)
    {
        if (position_to < position_from)
        {
//            throw "error, position from greater than position to";
        }
        else if (position_to < 0 || position_to >= size || position_from < 0 || position_from >= size)
        {
//            throw "Position from or position to is out of bounds";
        }
        std::vector<unsigned> vals = get_set(position_from, position_to);
        if (position_from == 0 && position_to == size-1)    // edge case, the whole set is returned
        {
            node* temp = this->tail;
            while (temp!= nullptr)
            {
                node*temp2 = temp;
                temp = temp->prev;
                delete temp2;
            }
            size = 0;
            head = nullptr;
            tail = nullptr;
            return vals;
        }
        int i = 0;
        node *temp = nullptr;
        if (lessThanMidpoint(position_from)) //finds location if less than midpoint of linked list
        {
            temp = head;
            while (i < position_from && temp->next != nullptr)
            {
                temp = temp->next;
                i++;
            }
        }
        else
        {
            temp = tail;
            i = size-1;
            while (i > position_from && temp->prev != nullptr)
            {
                temp = temp->prev;
                i--;
            }
        }
        node* tempP = temp; // temp will be gone?
        temp = temp->prev;
        for (int i = 0; i < position_to-position_from+1; i++) // removes values from old list
        {
//            node* tempT;
//            if (tempP ->next != nullptr)
//               tempT  = tempP->next;
            node* tempT = tempP->next;
            delete tempP;
            tempP = tempT;
        }

        if (position_from == 0) // edge case, head is deleted
        {
            head = tempP;
        }
        if (position_to == size-1) // edge case, tail is deleted
        {
            tail = temp;
            tail->next = nullptr;
        }
        size -= position_to - position_from + 1;
        doubly_linked_list to_return(vals);
        return to_return;
    }

// Swap two nodes in the list. USE POINTERS. Do not just swap the values!
    void doubly_linked_list::swap(unsigned position1, unsigned position2)
    {
        if (position1 < 0 || position1 > size - 1 || position2 < 0 || position2 > size - 1)
        {

        }
        else
        {
            swap_set(position1, position1, position2, position2);
        }
    }

// Swap two sets of cards. The sets are inclusive. USE POINTERS!
    void doubly_linked_list::swap_set(unsigned position1_from, unsigned position1_to, unsigned position2_from,
                                      unsigned position2_to)
    {
        if (position2_from < position1_from || position2_to < position1_to || position1_to > position2_from) // lots of checks to confirm valid input
        {
//            throw "invalid input into swap set, values out of order";
        }
        else if (position1_from > position1_to || position2_from > position2_to)
        {
//            throw "invalid input into swap set, values out of order";
        }
        else if (position1_from < 0 || position2_to > size)
        {
//            throw "invalid input into swap set, out of bounds";
        }
        if (position1_from == position1_to && position1_to == position2_from && position2_from == position2_to)
        {return;}
        // edge cases: this, mid, end don't exist
        doubly_linked_list set1,set2,end,mid;
        if (position2_to != size-1) // checks to make sure an end would exist ie. set2 does not go to end of list
        {
            end = split_set(position2_to+1,size-1);
        }
        set2 = split_set(position2_from,size-1);
        if (position1_to+1 != position2_from)   // checks to make sure a mid would exist ie. set1 does not go to start of set2
        {
            mid = split_set(position1_to + 1, size - 1);
        }
        set1 = split_set(position1_from, size - 1);
        if (position1_to + 1 == position2_from && size == 1)
        {
            head->next = nullptr;
            tail = head;
        }
        this->merge(set2);  // merges all the sets, if they are null the set is empty
        if (mid.head != nullptr)
            this->merge(mid);
        if (set1.head != nullptr)
            this->merge(set1);
        if (end.head != nullptr)
        this->merge(end);
    }

    // Overload operator=
    doubly_linked_list &doubly_linked_list::operator=(const doubly_linked_list &RHS) // writes over this and replaces with rhs
    {
        if (&RHS == this){
        return *this;
        }
        node *temp = head;
        while (head != nullptr)
        {
            head = head->next;
            delete temp;
            temp = head;
        }
        tail = nullptr;
        size = 0;
        temp = RHS.head;
        while (temp != nullptr)
        {
            append(temp->data);
            temp = temp->next;
        }
        return *this;
    }

    // Append the rhs to the end of the this list
    doubly_linked_list &doubly_linked_list::operator+=(const doubly_linked_list &RHS)
    {
        this->merge(RHS);    return *this;
    }

    unsigned doubly_linked_list::get_size()
    {
        return size;
    }

    bool doubly_linked_list::is_empty()
    {
        return !size;
    }

    bool doubly_linked_list::operator==(const doubly_linked_list &rhs) const {
        node * iterL=head, * iterR = rhs.head;
        while(iterL!= nullptr && iterR!= nullptr){
            if (iterL->data != iterR->data)
                return false;
            iterL = iterL->next;
            iterR = iterR->next;
        }
        return iterL == nullptr && iterR == nullptr;
    }

    std::string doubly_linked_list::to_string() {
        if(!head) return "";
        else {
            std::string output = "";
            output += std::to_string(head->data);
            node *to_return = head->next;
            while (to_return) {
                output += ", ";
                output += std::to_string(to_return->data);
                to_return = to_return->next;
            }
            return output;
        }
    }

    void doubly_linked_list::print() // prints list from head to tail
    {
        node *temp = head;
        std::cout << "Size: " << size << "\nnull <-> ";
        while (temp != nullptr)
        {
            std::cout << temp->data << " <-> ";
            temp = temp->next;
        }
        std::cout << "null\n";
    }

    void doubly_linked_list::printB() // prints list from tail to head (aka in reverse)
    {
        node *temp = tail;
        std::cout << "Size: " << size << "\nnull <-> ";
        while (temp != nullptr)
        {
            std::cout << temp->data << " <-> ";
            temp = temp->prev;
        }
        std::cout << "null\n";
    }

    bool doubly_linked_list::lessThanMidpoint(int location) // checks location against size to see if it is less than midpoint
    {
        int midpoint = size / 2;
        return location <= midpoint;
    }

}