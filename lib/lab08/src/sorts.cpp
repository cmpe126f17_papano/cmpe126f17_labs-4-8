#include "sorts.h"
//namespace lab8

lab6::doubly_linked_list split_recurse(lab6::doubly_linked_list input);
lab6::doubly_linked_list combine(lab6::doubly_linked_list start, lab6::doubly_linked_list end);
void quicksort(lab6::doubly_linked_list& input, int low,int high);
int partition(lab6::doubly_linked_list& input, int lo,int hi);
void rad_sort(lab6::doubly_linked_list& input,int size, int multiplier);


    lab6::doubly_linked_list sorts::insertion_sort(lab6::doubly_linked_list input, int iterations)
    {
        lab6::doubly_linked_list working_list = input;

        for (int i = 1; i < input.get_size() && i <= iterations; i++)
        {
            int next_val = working_list.get_data(i);
            for (int q = i; q > 0; q--)
            {
                if (next_val < working_list.get_data(q-1))
                {
                    working_list.swap(q-1,q);
                }
                else
                {break;}
            }
        }
        return working_list;
    }

    lab6::doubly_linked_list sorts::selection_sort(lab6::doubly_linked_list input, int iterations)
    {
        lab6::doubly_linked_list working_list = input;
        for (int i = 0; i < iterations; i++)
        {
            int pos_of_min = i;
            for (int q = i; q < working_list.get_size(); q++)
            {
                if (working_list.get_data(q) < working_list.get_data(pos_of_min))
                {
                    pos_of_min = q;
                }
            }
            working_list.swap(i,pos_of_min);
        }
        return working_list;
    }

    lab6::doubly_linked_list sorts::bubble_sort(lab6::doubly_linked_list input, int iterations)
    {
        lab6::doubly_linked_list working_list = input;
        for (int i = 0; i < iterations; i++)
        {
            for (int q = 0; q < working_list.get_size()-1; q++)
            {
                if (working_list.get_data(q) > working_list.get_data(q+1))
                    working_list.swap(q,q+1);
            }
        }
        return working_list;
    }

/*
 * Cocktail sort: go up and down the list, moving a hi's and lo's
 * URL: https://en.wikipedia.org/wiki/Cocktail_shaker_sort
 */
    lab6::doubly_linked_list sorts::cocktail_sort(lab6::doubly_linked_list input, int iterations)
    {
        lab6::doubly_linked_list working_list = input;
        bool swapped = false;
        for (int q = 0; q < working_list.get_size() && q < iterations; q++)
        {
            swapped = false;
            for (int i = 0; i < working_list.get_size() - 2; i++)
            {
                if (working_list.get_data(i) > working_list.get_data(i + 1))
                {
                    working_list.swap(i, i + 1);
                    swapped = true;
                }
            }
            if (!swapped)
                return working_list;
            swapped = false;
            for (int i = working_list.get_size() - 2; i >= 0; i--)
            {
                if (working_list.get_data(i) > working_list.get_data(i + 1))
                {
                    working_list.swap(i, i + 1);
                    swapped = true;
                }
            }
            if (!swapped)
                return working_list;
        }
        return working_list;
    }

/* quicksort:
 * pick pivot point, divide into arrays less than and greater than the pivot
 * recursively call quick sort until length = 1
 * recombine
*/
    lab6::doubly_linked_list sorts::quick_sort(lab6::doubly_linked_list input)
    {
        quicksort(input, 0,input.get_size()-1);
        return input;
    }
    void quicksort(lab6::doubly_linked_list& input, int low, int high)
    {
        if (low < high)
        {
            int parition_location = partition(input,low,high);
            quicksort(input,parition_location+1,high);
            quicksort(input, low, parition_location-1);
        }
    }
    int partition(lab6::doubly_linked_list& input, int lo,int hi)
    {
        int pivot_val = input.get_data(hi);
        int i = lo-1;
        for (int j = lo; j < hi; j++)
        {
            if (input.get_data(j) < pivot_val)
            {
                i++;
                input.swap(i,j);
            }
        }

        input.swap(i+1,hi);
        return i+1;
    }

    lab6::doubly_linked_list sorts::merge_sort(lab6::doubly_linked_list input)
    {
        return split_recurse(input);
    }

    lab6::doubly_linked_list split_recurse(lab6::doubly_linked_list input) // recursive division
    {
        if (input.get_size() == 1)
        {
            return input;
        }
        else
        {
            int mid = input.get_size()/2;
            lab6::doubly_linked_list end = input.split_set(mid, input.get_size()-1);
            input = split_recurse(input);
            end = split_recurse(end);
            return combine(input,end);
        }

    }
    lab6::doubly_linked_list combine(lab6::doubly_linked_list start, lab6::doubly_linked_list end)
    {
        lab6::doubly_linked_list to_return;
        if (start.get_size() > 0 && end.get_size() > 0)
        {
            while (start.get_size() > 0 && end.get_size() > 0)
            {
                if (start.get_data(0) < end.get_data(0))
                {
                    to_return.append(start.get_data(0));
                    start.remove(0);
                }
                else
                {
                    to_return.append(end.get_data(0));
                    end.remove(0);
                }
            }
        }
        if (start.get_size() == 0 && end.get_size() > 0)
        {
            while (end.get_size() != 0)
            {
                to_return.append(end.get_data(0));
                end.remove(0);
            }
        }
        else if (start.get_size() > 0 && end.get_size() == 0)
        {
            while(start.get_size() != 0)
            {
                to_return.append(start.get_data(0));
                start.remove(0);
            }
        }
        return to_return;
    }


    lab6::doubly_linked_list sorts::radix_sort(lab6::doubly_linked_list input)
    {
//        lab6::doubly_linked_list bucket[10] = {0}; // buckets
        int power_of_10 = 1; // 10^0
        int max = 0;
        for (int i = 0; i < input.get_size(); i++)
        {
            if (max < input.get_data(i))
                max = input.get_data(i);
        }
        while (max != 0)
        {

            rad_sort(input,input.get_size(),power_of_10);
            power_of_10*=10;
            max/=10;

        }
        return input;
    }
    void rad_sort(lab6::doubly_linked_list& input, int size, int multiplier)
    {
        lab6::doubly_linked_list bucket[10];
        for (int i = 0; i < size; i++)
        {
            int val = (input.get_data(i)/multiplier)%10;
            bucket[val].append(input.get_data(i));
        }
        lab6::doubly_linked_list output;
        for (int i = 0; i < 10; i++)
        {
            while (bucket[i].get_size() != 0)
            {
                output.append(bucket[i].get_data(0));
                bucket[i].remove(0);
            }
        }
        input = output;
    }