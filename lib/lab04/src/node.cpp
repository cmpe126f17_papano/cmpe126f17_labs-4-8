#include "node.h"
#include <iostream>
namespace lab4 {

    node::node()
    {
        data = 0;
        next = nullptr;
    }

    // Take in value and create a node
    node::node(int input)
    {
        next = nullptr;
        data = input;
    }

    // Takes in an array of values and creates the appropriate nodes
    node::node(int values[], int length)
    {
        node *temp = this;
        data = values[0];
        for (int i = 1; i < length; i++)
        {
            temp->next = new node(values[i]);
            temp = temp->next;
        }
    }

//     Default destructor
    node::~node()
    {
        // Hint: You don't want to just delete the current node. You need to keep track of what is next
    }

// Add a value to the end node
    void node::append(int input)
    {
        node *temp = this;
        while (temp->next != nullptr)
        {
            temp = temp->next;
        }
        temp->next = new node(input);
    }

// Add an array of values to the end as separate nodes
    void node::append(int inputs[], int length)
    {
        node *temp = this;
        while (temp->next != nullptr)
        {
            temp = temp->next;
        }
        for (int i = 0; i < length; i++)
        {
            temp->next = new node(inputs[i]);
            temp = temp->next;
        }
//    temp->data = inputs[length-1];
//    temp->next = nullptr;
    }

// Insert a new node after the given location
    node *node::insert(int location, int value)
    {
//        // Must return head pointer location
        node *temp = new node(value);
//        if (location == 0) // node created is new head
//        {
//            temp->next = this;
//            return temp;
//        }
//        else
//        {
            node *prev = new node;
            node *current = this;
            for (int i = 0; i < location+1; i++)
            {
                prev = current;
                current = current->next;
            }
            prev->next = temp;
            temp->next = current;
//        }
        return this;
    }

// Remove a node and link the next node to the previous node
    node *node::remove(int location)
    {
        // Must return head pointer location
        node *temp = new node;
        if (location == 0)
        {
            temp = this;
            if (this->next != nullptr)
            {
                temp = temp->next;
                return temp;
            }
        }
        else
        {
            node *after = this;
            for (int i = 0; i < location; i++)
            {
                temp = after;
                after = after->next;
            }
            temp->next = after->next;
            delete after;
            return this;
        }

    }

// Print all nodes
    void node::print()
    {
        node *temp = this;
        while (temp->next != nullptr)
        {
            std::cout << temp->data << " -> ";
            temp = temp->next;
        }
        std::cout << temp->data << " -> null";
    }

//Print the middle node
    void node::print_middle()
    {
        // HINT: Use a runner to traverse through the linked list at two different rates, 1 node per step
        //       and two nodes per step. When the faster one reaches the end, the slow one should be
        //       pointing to the middle
        node *faster = this;
        node *slower = this;
        while (faster->next != nullptr)
        {
            if (faster->next->next != nullptr)
            {
                faster = faster->next->next;
                slower = slower->next;
            }
            else
            {
                faster = faster->next;
            }
        }
        std::cout << slower->data;
    }

// Get the value of a given node
    int node::get_value(int location)
    {
        node *temp = this;
        for (int i = 0; i < location; i++)
        {
            if (temp->next != nullptr)
                temp = temp->next;
        }
        return temp->data;
    }

// Overwrite the value of a given node
    void node::set_data(int location, int value)
    {
        node *temp = this;
        for (int i = 0; i < location; i++)
        {
            if (temp->next != nullptr)
                temp = temp->next;
        }
        temp->data = value;
    }
}
